# react-redux-saga-swapi

This is test project for Ometria test. The project was generated using `create-react-app` and then I included a few libraries.

## Libraries used

* React
* React-Router 4 ( even though its not really required!, at first I thought, I will do the pagin with routes..)
* Redux
* Redux Saga
* ESlint
* Airbnb's ESlint rules
* React-debounce-input
  * This is very useful for letting user complete their typing before making API call. i.e. to avoid too many API calls as each key stroke simple alternative to RXJs
* React-loader-spinner: Pain free react spinner for loading state when search in progress


## Installation and run-locally

Please use `Yarn` to install dependencies and running the project. Its simple and efficient.
If you do not have yarn then, [get yarn from here](https://yarnpkg.com/lang/en/docs/install/#mac-stable)
Clone repo and run:

```
yarn && yarn start
```

## Completed features and rationale for CSS

* Type text in the textbox and then applciation will make a call to 
* swapi's people search API `https://swapi.co/api/people/?search={searchTerm}`
* If more than 10 records are returned, it will show paging component
* Tried not to use any CSS libraries or framework of components
* used all vanilla CSS/SCSS for the purpose of keeping it simple and not bloating the code with too many css components that come with their own CSS libraries!
* I have used mix of vanilla .css and .scss to demonstrate ability to use both and use of variables etc.
* Character Profile Cards are fully responsive for different form factors
* Paging information now shows currently displayed record counts

**Hope you will like it !!**
**Suggestions and comments are welcome.**

