
// eslint-disable-next-line import/prefer-default-export
export async function getStarWarsPerson(apiURL) {
  const response = await fetch(apiURL);
  return response.json();
}
