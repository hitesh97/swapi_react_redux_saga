import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getStarwarsSaga } from '../actions';
import { API_BASE_URL } from '../constants';
import Header from '../components/header/header';
import ProfileCardItem from '../components/profileCardItem';
import Pager from '../components/pager';
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import './index.css';

class Home extends Component {
  handlepagingButtonClick = (apiURL) => {
    const { getStarwarsSagaAction } = this.props;
    getStarwarsSagaAction(apiURL);
  };

  nameChanged = (e) => {
    const userInputValue = e.target.value;
    const apiURL = `${API_BASE_URL}${userInputValue}`;
    const { getStarwarsSagaAction } = this.props;
    getStarwarsSagaAction(apiURL);
  }

  handleBtnOnClick = () => {
    const apiURL = `${API_BASE_URL}${'o'}`;
    const { getStarwarsSagaAction } = this.props;
    getStarwarsSagaAction(apiURL);
  }

  render() {
    const { response, loading } = this.props;
    return (
      <div>
        <Header nameChanged={this.nameChanged} />
        <br />
        {!loading && response && response.results && response.results.length === 0
          && <div className="noResults">No results found !! Try another name!</div>
        }
        {!loading && response && response.results && response.results.length > 0
          && <Pager 
            response={{count: response.count, next: response.next, previous: response.previous, onNextClick: this.handlepagingButtonClick, onPreviousClick: this.handlepagingButtonClick}}
          />
        }
        {!loading && (
        <div className="grid-container">
            {response && response.results && response.results.length > 0
            && response.results.map(item => <ProfileCardItem {...item} key={item.name} />)}
        </div>
        )
        }
        {loading && <Loader
                  type="Puff"
                  color="gold"
                  height={300}
                  width={300}
                  visible={loading}
                  className="loader"
	      />}
        <br />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  response: state.starWarsReducer.response,
  loading: state.starWarsReducer.loading
});

const mapDispatchToProps = dispatch => ({
  getStarwarsSagaAction: apiURL => dispatch(getStarwarsSaga(apiURL))
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
