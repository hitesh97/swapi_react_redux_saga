import { put, takeEvery, call, all } from 'redux-saga/effects';

import { GET_STAR_WAR_PEOPLE_SAGA } from '../../constants';
import { setStarWarPeople } from '../../actions';
import { getStarWarsPerson } from '../../lib/api';

function* workerStarwarsSaga(action) {
  const response = yield call(getStarWarsPerson, action.apiURL);
  yield put(setStarWarPeople(response));
}

export default function* watchGetStarWarsSaga() {
  yield all([yield takeEvery(GET_STAR_WAR_PEOPLE_SAGA, workerStarwarsSaga)]);
}
