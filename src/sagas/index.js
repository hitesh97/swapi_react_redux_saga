import { all, fork } from 'redux-saga/effects';

import watchGetStarWarsSaga from './watchers/getUsers';

export default function* root() {
  yield all([
    fork(watchGetStarWarsSaga),
  ]);
}
