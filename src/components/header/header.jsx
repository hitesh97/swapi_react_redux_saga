import React from 'react';
import HeaderLogo from './headerLogo';
import {DebounceInput} from 'react-debounce-input';

const Header = (props) => (
    <div>
          <HeaderLogo />
          <h3 className="search-help">To search for your favourite StarWars Character type below and wait</h3>
          <DebounceInput debounceTimeout={400} onChange={props.nameChanged} placeholder="type here to search by name" autofocus />
    </div>
  );

  export default Header;