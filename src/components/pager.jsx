import React from 'react';

const decodeURLParams = search => {
  const hashes = search.slice(search.indexOf("?") + 1).split("&");
  return hashes.reduce((params, hash) => {
      const split = hash.indexOf("=");

      if (split < 0) {
          return Object.assign(params, {
              [hash]: null
          });
      }

      const key = hash.slice(0, split);
      const val = hash.slice(split + 1);

      return Object.assign(params, { [key]: decodeURIComponent(val) });
  }, {});
};

const computePageInfo = (nextPageNo, prevPageNo, count) => {
  let pagerInfo = {};
  const pageSize = 10;
  if(nextPageNo && nextPageNo > 0) {
    pagerInfo.MinItemNo = ((nextPageNo - 2 ) * pageSize) + 1;
    pagerInfo.MaxItemNo = ((nextPageNo - 1 ) * pageSize);
  }
  if(nextPageNo === 0 && prevPageNo && prevPageNo > 0) {
    pagerInfo.MinItemNo = ((prevPageNo) * pageSize) + 1;
    pagerInfo.MaxItemNo = count;
  }
  return pagerInfo;
}

const Pager = (props) => {
    const {count, next, previous, onNextClick, onPreviousClick} = props.response;
    const enabledClassName = 'button';
    const disabledClassName = 'buttonDisabled';
    const parsedNextUrl = decodeURLParams(next ? next : '');
    let nextPageNo = 0;
    if(next) {
      nextPageNo = 1;
    }
    if(parsedNextUrl && parsedNextUrl.page) {
      nextPageNo = parsedNextUrl.page;
    }
    const parsedPrevUrl = decodeURLParams(previous ? previous : '');
    let prevPageNo = 0;
    if(previous) {
      prevPageNo = 1;
    }
    if(parsedPrevUrl && parsedPrevUrl.page) {
      prevPageNo = parsedPrevUrl.page;
    }
    const pageCountInfo = computePageInfo(nextPageNo, prevPageNo, count);
    return (<div className="pagerContainer">
      <button className={previous? enabledClassName: disabledClassName} onClick={() => onNextClick(previous)} disabled={!previous} >&lt;&lt;&nbsp;previous</button>
      {count && <div className="separator">Total:<span>{count}</span><br /> {pageCountInfo && pageCountInfo.MinItemNo && <>Showing: {pageCountInfo.MinItemNo} - {pageCountInfo.MaxItemNo}</>}</div>}
      <button className={next? enabledClassName: disabledClassName} onClick={() => onPreviousClick(next)} disabled={!next} >next&nbsp;&gt;&gt;</button>
    </div>)};

  export default Pager;
