import React from 'react';
import PlaceHolder from './placeHolder';

const ProfileCardItem = (item) => (
<div className="card card-body" key={item.name}>
      <div className="card-header">
        <div className="card-profile">{<PlaceHolder />}</div>
        <h2>{item.name}</h2>
      </div>
      <ul className="card-title">
        <li>Gender: {item.gender}</li>
        <li>Height: {item.height}</li>
        <li>weight: {item.mass}kg</li>
      </ul>
    </div>
  );

  export default ProfileCardItem;