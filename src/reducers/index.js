import { combineReducers } from 'redux';
import { GET_STAR_WAR_PEOPLE_SAGA, SET_STAR_WAR_PEOPLE } from '../constants';

const initialState = { response: {} };

function starWarsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_STAR_WAR_PEOPLE_SAGA: {
      return {
        ...state,
        loading: true
      };
    }
    case SET_STAR_WAR_PEOPLE: {
      return {
        ...state,
        response: action.response,
        loading: false
      };
    }
    default:
      return state;
  }
}

export default combineReducers({
  starWarsReducer
});
