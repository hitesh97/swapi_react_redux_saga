import { SET_STAR_WAR_PEOPLE, GET_STAR_WAR_PEOPLE_SAGA } from '../constants';

export function getStarwarsSaga(apiURL) {
  return {
    type: GET_STAR_WAR_PEOPLE_SAGA,
    apiURL
  };
}

export function setStarWarPeople(response) {
  return {
    type: SET_STAR_WAR_PEOPLE,
    response
  };
}
