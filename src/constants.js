const API_BASE_URL = 'https://swapi.co/api/people/?search=';
const GET_STAR_WAR_PEOPLE_SAGA = 'GET_STAR_WAR_PEOPLE_SAGA';
const SET_STAR_WAR_PEOPLE = 'SET_STAR_WAR_PEOPLE';

export {  //eslint-disable-line
  API_BASE_URL,
  GET_STAR_WAR_PEOPLE_SAGA,
  SET_STAR_WAR_PEOPLE,
};
